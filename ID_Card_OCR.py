#from text_extraction import text_extraction_best as text_extract
#from digit_extraction import digit_extraction_full as digits_extract
from cv2 import cv2
import os
import numpy as np
import datetime
from skimage.measure import compare_ssim
import re
import sys
import dimensions_helper as dimensions
import Boundary_helper as bounds

# Initialize variables needed
currentScore = 0
Threshed = []
count = 0
Text = []
characters = []

images = 'Good/'

# Start the Extraction of RoIs from the Whole ID book
def Extract_ROIs(image):
    img = image
    Threshed = []
    # Get the dynamic dimensions from the get_dimensions function
    RoIs = dimensions.get_dimensions(1)
    Text = dimensions.get_dimensions(2)
    TextDim = RoIs[0]

    SurnameDim = Text[0]
    NameDim = Text[1]
    SexDim = Text[2]
    NationalityDim = Text[3]
    IdDim = Text[4]
    DobDim = Text[5]
    CobDim = Text[6]
    StatusDim = Text[7]

    # Get ID width and height
    (h, w) = img.shape[:2]

    # Extract Text Image from the Text Dimensions
    TextPic = img[round(int(h)*TextDim[0]) : round(int(h)*TextDim[1]), round(int(w)*TextDim[2]) : round(int(w)*TextDim[3])]

    (h, w) = TextPic.shape[:2]

    # Extract Info Images from the Text Image
    SurnamePic = TextPic[round(int(h)*SurnameDim[0]) : round(int(h)*SurnameDim[1]), round(int(w)*SurnameDim[2]) : round(int(w)*SurnameDim[3])]
    NamePic = TextPic[round(int(h)*NameDim[0]) : round(int(h)*NameDim[1]), round(int(w)*NameDim[2]) : round(int(w)*NameDim[3])]
    SexPic = TextPic[round(int(h)*SexDim[0]) : round(int(h)*SexDim[1]), round(int(w)*SexDim[2]) : round(int(w)*SexDim[3])]
    NationalityPic = TextPic[round(int(h)*NationalityDim[0]) : round(int(h)*NationalityDim[1]), round(int(w)*NationalityDim[2]) : round(int(w)*NationalityDim[3])]
    IdPic = TextPic[round(int(h)*IdDim[0]) : round(int(h)*IdDim[1]), round(int(w)*IdDim[2]) : round(int(w)*IdDim[3])]
    DobPic = TextPic[round(int(h)*DobDim[0]) : round(int(h)*DobDim[1]), round(int(w)*DobDim[2]) : round(int(w)*DobDim[3])]
    CobPic = TextPic[round(int(h)*CobDim[0]) : round(int(h)*CobDim[1]), round(int(w)*CobDim[2]) : round(int(w)*CobDim[3])]
    StatusPic = TextPic[round(int(h)*StatusDim[0]) : round(int(h)*StatusDim[1]), round(int(w)*StatusDim[2]) : round(int(w)*StatusDim[3])]

    RoIs = [SurnamePic, NamePic, SexPic, NationalityPic, IdPic, DobPic, CobPic, StatusPic]

    counter = 0
    test = False
    for img in RoIs:
        if counter == 0:
            boundary = bounds.Get_Best_Boundaries(img, 5, 3, 10, 11, test)
            Threshed.append(bounds.ROI_Thresh(img, boundary))

        elif counter == 1:
            boundary = bounds.Get_Best_Boundaries(img, 5, 3, 10, 12, test)
            Threshed.append(bounds.ROI_Thresh(img, boundary))

        elif counter == 2:
            boundary = bounds.Get_Best_Boundaries(img, 8, 4, 4, 6.2, test)
            Threshed.append(bounds.ROI_Thresh(img, boundary))

        elif counter == 3:
            boundary = bounds.Get_Best_Boundaries(img, 4, 3.2, 4.4, 14.5, test)
            Threshed.append(bounds.ROI_Thresh(img, boundary))

        elif counter == 4:
            boundary = bounds.Get_Best_Boundaries(img, 2, 3.2, 4.4, 26, test)
            Threshed.append(bounds.ROI_Thresh(img, boundary))

        elif counter == 5:
            boundary = bounds.Get_Best_Boundaries(img, 2, 4.5, 4.5, 19, test)
            Threshed.append(bounds.ROI_Thresh(img, boundary))

        elif counter == 6:
            boundary = bounds.Get_Best_Boundaries(img, 4, 4.5, 4.5, 14.5, test)
            Threshed.append(bounds.ROI_Thresh(img, boundary))

        elif counter == 7:
            boundary = bounds.Get_Best_Boundaries(img, 4, 5, 4.5, 14, test)
            Threshed.append(bounds.ROI_Thresh(img, boundary))

        counter += 1

    return Threshed

# Extract the text from the threshed ROIs
def Extract_Characters(Threshed):
    output = []
    count = 0
    for file in Threshed:
        count += 1
        # load the example image and convert it to grayscale
        if (count == 1) or (count == 4) or (count == 5):
            #OCR the Text from the image given
            #Name, out = text_extract(file, type)
            Name = ''
            out = ''
            length = len(Name)
            if length == 0 or length == 1:
                Text.append('Could not Read Date')
                output.append(out)
                continue

            if Name is not '':
                Text.append(Name)
                output.append(out)
            else:
                Text.append('Could Not Read Text')
                output.append(out)
        else:
            #OCR the Dates from the image given
            # digits = tool.image_to_string(img, lang='eng', builder=po.tesseract.DigitBuilder())
            #digits, out = digits_extract(file, type)
            digits = ''
            out = ''
            # cv2.imshow('image',out)
            # cv2.waitKey(0)
            # cv2.destroyAllWindows()

            # find length of digits string
            length = len(digits)
            # Move onto the next one if cannot find any digits
            if length == 0 or length == 1:
                Text.append('Could not Read Date')
                output.append(out)
                continue

            # Understand how many characters were found and organise them into their matching string
            if digits is not '':
                if len(digits) == 10:
                        digits = digits[:4]+ '-' + digits[5:7] + '-' + digits[8:]
                if len(digits) == 9:
                    if digits[4] == '7' or digits[4] == '2':
                        digits = digits[:4]+ '-' + digits[5:7] + '-' + digits[7:]
                    if digits[6] == '7' or digits[6] == '2':
                        digits = digits[:4]+ '-' + digits[4:6] + '-' + digits[7:]
                if len(digits) == 8:
                    digits = digits[:4]+ '-' + digits[4:6] + '-' + digits[6:]

                Text.append(digits)
                output.append(out)
            else:
                Text.append('Could not Read Date')
                output.append(out)
    return Text, output

def Extract_All_Characters(file):
    #Read in image in grayscale
    imgLoad = cv2.imread(file)
    characters = []
    finalOut = []

    Threshed = Extract_ROIs(imgLoad)
    Text, output = Extract_Characters(Threshed)

    # Used to create a jsonString for API calls
    count = 0
    for object in Text:
        if count == 0:
            characters.append(['Country', object])
        if count == 1:
            characters.append(['DoI', object])
        if count == 2:
            characters.append(['DoB', object])
        if count == 3:
            characters.append(['Name', object])
        if count == 4:
            characters.append(['Surname', object])
        count += 1

    count = 0
    for object in output:
        if count == 0:
            finalOut.append(['Country Image', object])
        if count == 1:
            finalOut.append(['DoI Image', object])
        if count == 2:
            finalOut.append(['DoB Image', object])
        if count == 3:
            finalOut.append(['Name Image', object])
        if count == 4:
            finalOut.append(['Surname Image', object])
        count += 1

    # return consolidated results from the functions above in array form
    return characters, finalOut
    #return Threshed ---------------TESTER

files = []

for filename in os.listdir(images):
    files.append(images + filename)
for file in files:
    Threshed = Extract_All_Characters(file)
    for item in Threshed:
        cv2.imshow('ROI', item)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
