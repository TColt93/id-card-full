#python3.6 App.py -i "michael meyer.jpg"

import os
from cv2 import cv2
import numpy as np
import argparse
from random import randint
import os
import ID_Card_OCR as OCR
import homography
import ID_Card_Validations as Validations
import json
import datetime
 
# Initialize the argeparse for the required inputs
ap = argparse.ArgumentParser()
predictions = []
outputs = []
ap.add_argument("-is", "--images",
    default=None, help="image batch")
ap.add_argument("-i", "--image",
    default=None, help="single image")
ap.add_argument("-r", "--returns",
    default=True, help="return images")
ap.add_argument("-f", "--function",
    default="Full", help="function to initialize")
args = vars(ap.parse_args())

files = []

# Pass through the arguments and make a decision whether or not to iterate through a folder or append a file
if  args["images"] != None:
    directory = args["images"]
    for filename in os.listdir(directory):
        files.append(directory + filename)
elif args["image"] != None:
    directory = args["image"]
    files.append(directory)

# Create a returns variable to understand whether or not the call expects image outputs
returns = args["returns"]

# Create a function variable to call the correct function from the arguments given
function = args["function"]

# This function is used to do validations and OCR of the given files
# Returns the consolidated predictions and consolidated image arrays
def Full_ID(files):
    for file in files:
        # Read in the orginial image from file path in color
        img = cv2.imread(file, cv2.IMREAD_COLOR)
        outputs.append(['Initial Image', img])

        # Input the image we have just read into the Homography function
        # Return best image and type of ID Book
        bestImage = homography.Perfect_Image(img)

        # Input the best image and ID Type to the OCR function to extract all text and numbers
        # Return the OCRImg array with bounding boxes and character predictions and the characters extracted in json format
        jsonOCR, OCRImgs = OCR.Extract_All_Characters(bestImage)
        outputs.append(['OCR Regions', OCRImgs])
        predictions.append(['OCR Predictions', jsonOCR])

        # Input the best image and ID Type to the Validation function to validate all dates and Nationality and perform a barcode check
        # Return the ValidateImg arrays with bounding boxes for all digits extractions (3) and the characters extracted in json format
        jsonValidate, ValidateImgs = Validations.Validate_ID(bestImage)
        # for image in ValidateImgs:
        #     cv2.imshow('image',image[0][1])
        #     cv2.waitKey(0)
        #     cv2.destroyAllWindows()
        outputs.append(['Validation Regions', ValidateImgs])
        predictions.append(['Validation Predictions', jsonValidate])

    return predictions, outputs


# This function is used to OCR the given files
# Returns the consolidated predictions and consolidated image arrays
def OCR_ID(files):
    for file in files:
        # Read in the orginial image from file path in color
        img = cv2.imread(file, cv2.IMREAD_COLOR)
        outputs.append('Initial Image', img)

        # Input the image we have just read into the Homography function
        # Return best image and type of ID Book
        bestImage = homography.Perfect_Image(img)

        # Input the best image and ID Type to the OCR function to extract all text and numbers
        # Return the OCRImg array with bounding boxes and character predictions and the characters extracted in json format
        jsonOCR, OCRImgs = OCR.Extract_All_Characters(bestImage)
        outputs.append('OCR Regions', OCRImgs)
        predictions.append('OCR Predictions', jsonOCR)

    return predictions, outputs


# This function is used to do validations of the given files
# Returns the consolidated predictions and consolidated image arrays
def Validate_ID(files):
    for file in files:
        # Read in the orginial image from file path in color
        img = cv2.imread(file, cv2.IMREAD_COLOR)
        outputs.append('Initial Image', img)

        # Input the image we have just read into the Homography function
        # Return best image and type of ID Book
        bestImage, Type = homography.Perfect_Image(img)

        # Input the best image and ID Type to the Validation function to validate all dates and Nationality and perform a barcode check
        # Return the ValidateImg arrays with bounding boxes for all digits extractions (3) and the characters extracted in json format
        jsonValidate, ValidateImgs = Validations.Validate_ID(bestImage)
        outputs.append('Validation Regions', ValidateImgs)
        predictions.append('Validation Predictions', jsonValidate)

    return predictions, outputs

def Function_Chooser(files, returns, function):
    OUTPUT = []
    IMAGES = []
    if function == "Full":
        predictions, outputs = Full_ID(files)
    elif function == "OCR":
        predictions, outputs = OCR_ID(files)
    elif function == "Validate":
        predictions, outputs = Validate_ID(files)
    
    if returns == False:
        OUTPUT.append(predictions)
        OUTPUT.append(['DateTime', str(datetime.datetime.now())])
    else:
        OUTPUT.append(predictions)
        OUTPUT.append(['DateTime', str(datetime.datetime.now())])
        IMAGES.append(outputs)
    
    Encoded_Outputs = json.dumps(OUTPUT)
    for out in OUTPUT:
        print(out)
    return Encoded_Outputs

# Pass the argument variables through to the function chooser
Function_Chooser(files, returns, function)
        
        
    

# Write aligned image to disk.
# print("Image is of Type: ", Name)
# folder = '%s_%s_best10_6/' % (str(MAX_FEATURES), str(GOOD_MATCH_PERCENT))
# if not os.path.exists(folder):
#     os.makedirs(folder)
# path = folder + str(count) + '_' + Name + '.png'
# cv2.imwrite(path , bestImage)
# count += 1