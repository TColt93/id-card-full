from keras.preprocessing.image import img_to_array
from keras.models import load_model
from pyimagesearch.utils.captchahelper import preprocess
from skimage.measure import compare_ssim
from imutils import contours
from imutils import paths
import numpy as np
import argparse
import imutils
from cv2 import cv2

def extract_digits(image, type, rnd):
	# Choose the appropriate digit model when dealing with the two types of ID Books
	if type == 0:
		model = load_model('output/numnetNew.hdf5')
	if type == 1:
		model = load_model('output/numnet1.hdf5')

	# Make sure that the dimensions of the images are all set to the same height to get more accurate results
	shape = image.shape
	height = 125
	r = height / float(shape[0])
	dim = (int(shape[1] * r), height)

	image = cv2.resize(image, dim, interpolation = cv2.INTER_AREA)

	shape = image.shape
	# Find the shape and ensure that the size has depth, else do not gray (causes errors)
	if len(shape) == 3:
		gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	else:
		gray = image

	if rnd == 0:
		gray = cv2.copyMakeBorder(gray, 5, 5, 5, 5,
			cv2.BORDER_CONSTANT, value=[255,255,255])
	if rnd == 1:
		gray = cv2.copyMakeBorder(gray, 20, 20, 20, 20,
			cv2.BORDER_CONSTANT, value=[255,255,255])
	original = cv2.copyMakeBorder(image, 5, 5, 5, 5,
		cv2.BORDER_CONSTANT, value=[255,255,255])

	# threshold the image to reveal the digits
	thresh = cv2.threshold(gray, 0, 255,
		cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]

	# find contours in the image, keeping only the four largest ones,
	# then sort them from left-to-right
	cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
		cv2.CHAIN_APPROX_SIMPLE)
	cnts = cnts[0] if imutils.is_cv2() else cnts[1]
	cnts = sorted(cnts, key=cv2.contourArea, reverse=True)
	cnts = contours.sort_contours(cnts)[0]

	# initialize the output image as a "grayscale" image with 3
	# channels along with the output predictions
	output = cv2.merge([gray] * 3)
	predictions = []
	hts = []
	wts = []
	if cnts != []:
		cnts = contours.sort_contours(cnts)[0]
		# loop over the contours
		for c in cnts:
			# compute the bounding box for the contour then extract the
			# digit
			(x, y, w, h) = cv2.boundingRect(c)
			if h < 7:
				continue
			roi = gray[y - 5:y + h + 5, x - 5:x + w + 5]
			if roi.size == 0:
				continue

			hig,wid = roi.shape[:2]
			blank_image = np.zeros((hig,wid), np.uint8)

			rt, blank_image = cv2.threshold(blank_image, 255, 255, cv2.THRESH_BINARY)

			blank_image = cv2.resize(blank_image, (wid, hig))

			(score, diff) = compare_ssim(roi, blank_image, full=True, multichannel=True)
			if score > 0.3:
				continue

			height,width = roi.shape[:2]
			rois = []

			# If width is greater than the height, there is a potential
			# two digits are touching and need to be divided into two
			# and checked again
			if width > height:
				# Get the two new images with potential digits
				width1 = round(width/2)
				if width1 > height:
					continue
				roi1 = roi[0:height, 0:width1]
				roi2 = roi[0:height, width1:width1 + width]
				roi1 = cv2.copyMakeBorder(roi1, 5, 5, 5, 5,
					cv2.BORDER_CONSTANT, value=[255,255,255])
				roi2 = cv2.copyMakeBorder(roi2, 5, 5, 5, 5,
					cv2.BORDER_CONSTANT, value=[255,255,255])
				rois.append(roi1)
				rois.append(roi2)

				count = 0
				for r in rois:
					# pre-process the ROI and classify it then classify it
					r = preprocess(r, 28, 28)
					r = np.expand_dims(img_to_array(r), axis=0)
					pred = model.predict(r).argmax(axis=1)[0]
					if count == 0:
						predictions.append(str(pred))
						hts.append([[h,y,y+h]])
						wts.append([[w/2,x,x+int(w/2)]])

						# draw the prediction on the output image
						cv2.rectangle(output, (x - 2, y - 2),
							(x + int(w/2) + 4, y + h + 4), (0, 255, 0), 1)
						cv2.putText(output, str(pred), (x - 5, y - 5),
							cv2.FONT_HERSHEY_SIMPLEX, 0.55, (0, 255, 0), 2)

					if count == 1:
						predictions.append(str(pred))
						hts.append([[h,y,y+h]])
						wts.append([[w,x,x + int(w/2) +int(w/2)]])

						# draw the prediction on the output image
						cv2.rectangle(output, (x - 2, y - 2),
							(x+int(w/2) + int(w/2) + 4, y + h + 4), (0, 255, 0), 1)
						cv2.putText(output, str(pred), (x+int(w/2) - 5, y - 5),
							cv2.FONT_HERSHEY_SIMPLEX, 0.55, (0, 255, 0), 2)

					count += 1

			else:
				# pre-process the ROI and classify it then classify it
				roi = preprocess(roi, 28, 28)
				roi = np.expand_dims(img_to_array(roi), axis=0)
				pred = model.predict(roi).argmax(axis=1)[0]
				predictions.append(str(pred))
				hts.append([[h,y,y+h]])
				wts.append([[w,x,x+w]])

				# draw the prediction on the output image
				cv2.rectangle(output, (x - 2, y - 2),
					(x + w + 4, y + h + 4), (0, 255, 0), 1)
				cv2.putText(output, str(pred), (x - 5, y - 5),
					cv2.FONT_HERSHEY_SIMPLEX, 0.55, (0, 255, 0), 2)

	# cv2.imshow('img', output)
	# cv2.waitKey(0)
	# cv2.destroyAllWindows()

	return hts, wts, predictions, output, original

def digit_extraction_full(image, type):

	hts, wts, predictions, output, original = extract_digits(image, type, 0)

	height = []
	highest = 0
	start = []
	end = []
	counter = 0

	# Calculate height median of the text characters
	for ht in hts:
		h = ht[0][0]
		height.append(h)
		counter += 1
		hs = ht[0][1]
		he = ht[0][2]
		start.append(hs)
		end.append(he)
	sortd = sorted(height.copy(), reverse = True)
	if len(height) >= 8:
		highest = sortd[2]
	else:
		highest = sortd[1]

	# Calculate the start and end points for the characters that fit into the median height
	count = 0
	starter = 0
	ender = 0
	for h in height:
		s = start[count]
		e = end[count]
		if h == highest:
			starter = s
			ender = e
			break
		count += 1

	# Match those characters that fit into the height range and start and end point ranges
	count = 0
	hts1 = []
	wts1 = []
	hPrevStart = 0
	hPrevEnd = 0
	characters = []
	for h in height:
		s = start[count]
		e = end[count]
		if len(hts1) != 0:
			hPrevHeight = hts1[len(hts1)-1][0][0]
			lng = len(hts1)-1
			for h in height:
				if hPrevHeight < highest * 78/100:
					lng = lng - 1
					hPrevHeight = hts1[lng][0][0]
				else:
					hPrevStart = hts1[lng][0][1]
					hPrevEnd = hts1[lng][0][2]
					break
			if (hPrevStart != 0) and ((s >= starter - 10 and s <= ender + 10) and (e >= starter - 10 and e <= ender + 10)) or ((s >= hPrevStart * 78/100 and s <= hPrevEnd * 115/100) and (e >= hPrevStart * 78/100 and e <= hPrevEnd * 115/100)):
				characters.append(predictions[count])
				hts1.append(hts[count])
				wts1.append(wts[count])
		else:
			if ((s >= starter - 10 and s <= ender + 10) and (e >= starter - 10 and e <= ender + 10)) and h >= highest * 78/100:
				characters.append(predictions[count])
				hts1.append(hts[count])
				wts1.append(wts[count])
		count += 1

	# Initialize variables start creating the new character array
	counter = 0
	new_preds = []
	new_hs = []
	new_ws = []
	for pred in characters:
		if counter == len(characters) - 1:
			new_preds.append(characters[counter])
			new_hs.append(hts1[counter])
			new_ws.append(wts1[counter])
			break
		# break down the prediction's corresponding region edges
		new_preds.append(characters[counter])
		ht = hts1[counter]
		ht2 = hts1[counter+1]
		wt = wts1[counter]
		wt2 = wts1[counter+1]
		new_hs.append(ht)
		new_ws.append(wt)
		# individual points of interest
		h = ht[0][0]
		h2 = ht2[0][0]
		ws = wt[0][1]
		ws2 = wt2[0][1]
		we = wt[0][2]
		we2 = wt2[0][2]
		hs = ht[0][1]
		hs2 = ht2[0][1]
		# find the digits found with possible need of dilation for better results
		if (h < (highest * 75/100)) and (h2 < (highest * 75/100)) and (h > (highest * 25/100)) and (h2 > (highest * 25/100)) and (ws2-ws < 15) and (hs >= (starter * 95/100) and hs2 >= (starter * 95/100)) and (hs < (ender * 95/100) and hs2 < (ender * 95/100)):
			farthest = 0
			if we >= we2:
				farthest = int(we)
			if we2 >= we:
				farthest = int(we2)
			# create region of interest to find text
			roi = original[starter-2:ender+2, ws-2:farthest]

			# cv2.imshow('img', roi)
			# cv2.waitKey(0)

			# cv2.imshow('img', roi)
			# cv2.waitKey(0)
			# cv2.destroyAllWindows()

			# Create a large kernel because it is an erode single digit we are looking for
			# inverse the image to close all the open spaces within the text
			# Re-inverse to get the better quality image back to the black on white
			kernel = kernel = np.ones((3,3),np.uint8)
			roi = cv2.bitwise_not(roi)
			roi = cv2.morphologyEx(roi, cv2.MORPH_CLOSE, kernel)
			ret,roi = cv2.threshold(roi,127,255,cv2.THRESH_TOZERO)
			roi = cv2.bitwise_not(roi)

			# cv2.imshow('img', roi)
			# cv2.waitKey(0)
			# cv2.destroyAllWindows()

			# This step is used to ensure that the new image found correlates to the old text found
			roi_hts, roi_wts, roi_predictions, out, orig = extract_digits(roi, type, 1)
			c = 0

			# cv2.imshow('img', out)
			# cv2.waitKey(0)
			
			for h in roi_hts:
				if h[0][0] >= (highest * 90/100) and (roi_wts[c][0][0] > 15):
					new_preds.remove(new_preds[len(new_preds)-1])
					new_preds.append(roi_predictions[c])
					new_hs[counter][0][0] = highest
					new_hs[counter][0][1] = starter
					new_hs[counter][0][2] = ender
					new_ws[counter][0][1] = ws
					new_ws[counter][0][0] = farthest - ws
					new_ws[counter][0][2] = farthest
					counter += 1
				c += 1

		counter += 1

	finalHeights = []
	finalCharacters = []
	finalWidths = []
	counter = 0

	# Loop through the predicted text characters and find the ones that match the median text height
	for pred in new_preds:
		ht = new_hs[counter]
		h = ht[0][0]
		removed = False
		if h > round(highest - 12) and (pred == '4' and type == 0):
			removed = False
		elif h < round(highest - 9):
			removed = True
		if removed != True:
			finalHeights.append(ht)
			finalCharacters.append(pred)
			finalWidths.append(new_ws[counter])
		counter += 1

	counter = 0
	chars = []
	widths = []
	heights = []
	length = len(finalCharacters)
	# Run through the widths of the chars
	for wt in finalWidths:
		# Append the next array item for: widths, heights and chars
		chars.append(finalCharacters[counter])
		widths.append(wt)
		heights.append(finalHeights[counter])
		# Check if not end of array
		if counter + 1 != length:
			# If not, grab the next character and widths of that character
			wt2 = finalWidths[counter+1]
			# Perform the tesst:
			tester = wt2[0][1] - wt[0][2]
			# (first character end and second character start point are
			# more than 15 pixels away from eachother)
			# The big difference is due to the differences in image qualities
			if (tester > 17):
				# Add space to chars
				chars.append(' ')

		# Add to the counter
		counter += 1

	prediction = ''
	for pred in chars:
		prediction = prediction + pred

	return prediction, output