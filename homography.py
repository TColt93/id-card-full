from cv2 import cv2
import numpy as np
import argparse
from random import randint
import os
 
MAX_FEATURES = 2000
GOOD_MATCH_PERCENT = 0.075

def align_images(im1, im2):
    #Make the Template Image the standard size to ensure the best results when finding which Template to use
    shape = im2.shape
    height = 650
    r = height / float(shape[0])
    dim = (int(shape[1] * r), height)

    im2 = cv2.resize(im2, dim, interpolation = cv2.INTER_AREA)


    # Convert images to grayscale
    im1Gray = cv2.cvtColor(im1, cv2.COLOR_BGR2GRAY)
    im2Gray = cv2.cvtColor(im2, cv2.COLOR_BGR2GRAY)

    # Detect ORB features and compute descriptors.
    orb = cv2.ORB_create(MAX_FEATURES)
    keypoints1, descriptors1 = orb.detectAndCompute(im1Gray, None)
    keypoints2, descriptors2 = orb.detectAndCompute(im2Gray, None)

    # Match features.
    matcher = cv2.DescriptorMatcher_create(cv2.DESCRIPTOR_MATCHER_BRUTEFORCE_HAMMING)
    matches = matcher.match(descriptors1, descriptors2, None)

    # Sort matches by score
    matches.sort(key=lambda x: x.distance, reverse=False)

    # Remove not so good matches
    numGoodMatches = int(len(matches) * GOOD_MATCH_PERCENT)
    matches = matches[:numGoodMatches]

    # Draw top matches (Use for testing purposes)
    # imMatches = cv2.drawMatches(im1, keypoints1, im2, keypoints2, matches, None)
    # cv2.imwrite("matches.jpg", imMatches)

    # Extract location of good matches
    points1 = np.zeros((len(matches), 2), dtype=np.float32)
    points2 = np.zeros((len(matches), 2), dtype=np.float32)

    for i, match in enumerate(matches):
        points1[i, :] = keypoints1[match.queryIdx].pt
        points2[i, :] = keypoints2[match.trainIdx].pt

    # Find homography
    h, mask = cv2.findHomography(points1, points2, cv2.RANSAC)

    # Use homography
    height, width, channels = im2.shape
    im1Reg = cv2.warpPerspective(im1, h, (width, height))

    return im1Reg

images = 'Cards/'
files = []

for filename in os.listdir(images):
    files.append(images + filename)

def Perfect_Image(files):
    for file in files:
        # Read image to be aligned
        im = cv2.imread(file, cv2.IMREAD_COLOR)

        # Read reference image post 2010
        refFilename = "Templates/ID_Card_Template.png"
        Reference = cv2.imread(refFilename, cv2.IMREAD_COLOR)

        bestImage = align_images(im, Reference)

    return bestImage