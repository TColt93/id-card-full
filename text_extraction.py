# import the necessary packages
from keras.preprocessing.image import img_to_array
from keras.models import load_model
from pyimagesearch.utils.captchahelper import preprocess
from imutils import contours
from imutils import paths
import numpy as np
import argparse
import imutils
from cv2 import cv2
import re

def find_text(image, type, rnd):
	# Choose the appropriate digit model when dealing with the two types of ID Books
	if type == 0:
		model = load_model('output/textnet.hdf5')
	if type == 1:
		model = load_model('output/textnet.hdf5')

	# load the image and convert it to grayscale, then pad the image
	# to ensure digits caught only the border of the image are
	# retained
	shape = image.shape
	if len(shape) == 3:
		gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	else:
		gray = image
	if rnd == 0:
		gray = cv2.copyMakeBorder(gray, 5, 5, 5, 5,
			cv2.BORDER_CONSTANT, value=[255,255,255])
	if rnd == 1:
		gray = cv2.copyMakeBorder(gray, 20, 20, 20, 20,
			cv2.BORDER_CONSTANT, value=[255,255,255])
	original = cv2.copyMakeBorder(image, 5, 5, 5, 5,
		cv2.BORDER_CONSTANT, value=[255,255,255])

	# threshold the image to reveal the digits
	thresh = cv2.threshold(gray, 0, 255,
		cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]

	# find contours in the image / when you are only the 13 largest ones, use [:13],
	# then sort them from left-to-right
	cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
		cv2.CHAIN_APPROX_SIMPLE)
	cnts = cnts[0] if imutils.is_cv2() else cnts[1]
	cnts = sorted(cnts, key=cv2.contourArea, reverse=True)

	# initialize the output image as a "grayscale" image with 3
	# channels along with the output predictions
	output = cv2.merge([thresh] * 3)
	predictions = []
	hts = []
	wts = []

	if cnts != []:
		cnts = contours.sort_contours(cnts)[0]
		# loop over the contours
		for c in cnts:
			heights = []
			widths = []
			predicts = []
			# compute the bounding box for the contour then extract the
			# digit
			(x, y, w, h) = cv2.boundingRect(c)

			if h < 10:
				continue

			# Create the region of interest around the potential digits found
			roi = gray[y-5:y + h+5, x-5:x + w+5]

			# Get the height and weight of the RoI and initialize rois for
			# the new region with potential digits
			height,width = roi.shape[:2]
			rois = []

			# If width is greater than the height, there is a potential
			# two digits are touching and need to be divided into two
			# and checked again
			if width > height*1.25:
				# Get the two new images with potential digits
				width1 = round(width/2)
				roi1 = roi[0:height, 0:width1]
				roi2 = roi[0:height, width1:width1 + width]
				roi1 = cv2.copyMakeBorder(roi1, 5, 5, 5, 5,
					cv2.BORDER_CONSTANT, value=[255,255,255])
				roi2 = cv2.copyMakeBorder(roi2, 5, 5, 5, 5,
					cv2.BORDER_CONSTANT, value=[255,255,255])
				rois.append(roi1)
				rois.append(roi2)
				count = 0
				for r in rois:
					# pre-process the ROI and classify it then classify it
					r = preprocess(r, 28, 28)
					r = np.expand_dims(img_to_array(r), axis=0)
					pred = model.predict(r).argmax(axis=1)[0]
					if count == 0:
						predicts.append(Alphabet[pred])
						heights.append([h,y,y+h])
						widths.append([w/2,x,x+int(w/2)])

						# draw the prediction on the output image
						cv2.rectangle(output, (x - 2, y - 2),
							(x + int(w/2) + 4, y + h + 4), (0, 255, 0), 1)
						cv2.putText(output, Alphabet[pred], (x - 5, y - 5),
							cv2.FONT_HERSHEY_SIMPLEX, 0.55, (0, 255, 0), 2)

					if count == 1:
						predicts.append(Alphabet[pred])
						heights.append([h,y,y+h])
						widths.append([w/2,x+int(w/2),x+w])

						# draw the prediction on the output image
						cv2.rectangle(output, (x - 2, y - 2),
							(x+int(w/2) + int(w/2) + 4, y + h + 4), (0, 255, 0), 1)
						cv2.putText(output, Alphabet[pred], (x+int(w/2) - 5, y - 5),
							cv2.FONT_HERSHEY_SIMPLEX, 0.55, (0, 255, 0), 2)

					count += 1
			else:
				# pre-process the ROI and classify it then classify it
				roi = preprocess(roi, 28, 28)
				roi = np.expand_dims(img_to_array(roi), axis=0)
				pred = model.predict(roi).argmax(axis=1)[0]
				predicts.append(Alphabet[pred])
				heights.append([h,y,y+h])
				widths.append([w,x,x+w])

				# draw the prediction on the output image
				# necessary only for preview purposes
				cv2.rectangle(output, (x, y),
					(x + w, y + h), (0, 255, 0), 1)
				cv2.putText(output, Alphabet[pred], (x, y),
					cv2.FONT_HERSHEY_SIMPLEX, 0.55, (0, 255, 0), 2)

			for h in heights:
				hts.append([h])
			for w in widths:
				wts.append([w])
			for p in predicts:
				predictions.append(p)

	# cv2.imshow('img', output)
	# cv2.waitKey(0)
	# cv2.destroyAllWindows()

	# return all heights, weights, predicted letters and images
	return hts, wts, predictions, output, original

# This method uses the text found in the previous 'find_text' step to identify
# the largest text in the Region of Interest which is the text we are looking for
def best_text(hts, wts,predictions, original, type):
	# Initialize the variables needed to get best text
	height = []
	highest = 0
	start = []
	end = []
	counter = 0

	# Calculate height median of the text characters
	for ht in hts:
		h = ht[0][0]
		height.append(h)
		counter += 1
		hs = ht[0][1]
		he = ht[0][2]
		start.append(hs)
		end.append(he)
	# sort the heights in order to get the biggest height in the array
	sortd = sorted(height.copy(), reverse = True)
	# Use the second digits in case there is an abnormality and an incorrect text block is chosen
	highest = sortd[1]

	# Calculate the start and end points for the characters that fit into the median height
	count = 0
	starter = 0
	ender = 0
	for h in height:
		s = start[count]
		e = end[count]
		if h == highest:
			starter = s
			ender = e
			break
		count += 1

	# Match those characters that fit into the height range and start and end point ranges
	count = 0
	preds1 = []
	hts1 = []
	wts1 = []
	hPrevStart = 0
	hPrevEnd = 0
	for h in height:
		s = start[count]
		e = end[count]
		if len(hts1) != 0:
			hPrevHeight = hts1[len(hts1)-1][0][0]
			lng = len(hts1)-1
			for h in height:
				if hPrevHeight < highest * 78/100:
					lng = lng - 1
					hPrevHeight = hts1[lng][0][0]
				else:
					hPrevStart = hts1[lng][0][1]
					hPrevEnd = hts1[lng][0][2]
					break
			if (hPrevStart != 0) and ((s >= starter - 10 and s <= ender + 10) and (e >= starter - 10 and e <= ender + 10)) or ((s >= hPrevStart * 78/100 and s <= hPrevEnd * 115/100) and (e >= hPrevStart * 78/100 and e <= hPrevEnd * 115/100)):
				preds1.append(predictions[count])
				hts1.append(hts[count])
				wts1.append(wts[count])
		else:			
			if ((s >= starter - 10 and s <= ender + 10) and (e >= starter - 10 and e <= ender + 10)) and h >= highest * 78/100:
				preds1.append(predictions[count])
				hts1.append(hts[count])
				wts1.append(wts[count])
		count += 1

	# Initialize variables start creating the new character array
	counter = 0
	new_preds = []
	new_hs = []
	new_ws = []
	for pred in preds1:
		if counter == len(preds1) - 1:
			new_preds.append(preds1[counter])
			new_hs.append(hts1[counter])
			new_ws.append(wts1[counter])
			break
		# break down the prediction's corresponding region edges
		new_preds.append(preds1[counter])
		ht = hts1[counter]
		ht2 = hts1[counter+1]
		wt = wts1[counter]
		wt2 = wts1[counter+1]
		new_hs.append(ht)
		new_ws.append(wt)
		# individual points of interest
		h = ht[0][0]
		h2 = ht2[0][0]
		ws = wt[0][1]
		ws2 = wt2[0][1]
		we = wt[0][2]
		we2 = wt2[0][2]
		hs = ht[0][1]
		hs2 = ht2[0][1]
		# find the digits found with possible need of dilation for better results
		if (h < (highest * 75/100)) and (h2 < (highest * 75/100)) and (h > (highest * 25/100)) and (h2 > (highest * 25/100)) and (ws2-ws < 10) and (hs >= (starter * 95/100) and hs2 >= (starter * 95/100)) and (hs < (ender * 95/100) and hs2 < (ender * 95/100)):
			farthest = 0
			if we >= we2:
				farthest = int(we)
			if we2 >= we:
				farthest = int(we2)
			# create region of interest to find text
			roi = original[starter-2:ender+2, ws-2:farthest]

			# Create a large kernel because it is an erode single digit we are looking for
			# inverse the image to close all the open spaces within the text
			# Re-inverse to get the better quality image back to the black on white
			kernel = kernel = np.ones((9,9),np.uint8)
			roi = cv2.bitwise_not(roi)
			roi = cv2.morphologyEx(roi, cv2.MORPH_CLOSE, kernel)
			ret,roi = cv2.threshold(roi,127,255,cv2.THRESH_TOZERO)
			roi = cv2.bitwise_not(roi)

			# This step is used to ensure that the new image found correlates to the old text found
			roi_hts, roi_wts, roi_predictions, out, orig = find_text(roi, type, 1)
			c = 0
			for h in roi_hts:
				if h[0][0] >= (highest * 90/100) and (roi_wts[c][0][0] > 15):
					new_preds.remove(new_preds[len(new_preds)-1])
					new_preds.append(roi_predictions[c])
					new_hs[counter][0][0] = highest
					new_hs[counter][0][1] = starter
					new_hs[counter][0][2] = ender
					new_ws[counter][0][1] = ws
					new_ws[counter][0][0] = farthest - ws
					new_ws[counter][0][2] = farthest
					counter += 1
				c += 1

		counter += 1

	# Initialize your return values
	finalHeights = []
	finalWidths = []
	finalCharacters = []
	counter = 0
	chars = ''

	# Loop through the predicted text characters and find the ones that match the median text height
	for pred in new_preds:
		ht = new_hs[counter]
		wt = new_ws[counter]
		h = ht[0][0]
		if h < round(highest - 12):
			chars = chars + pred + '-'
		else:
			finalHeights.append(ht)
			finalWidths.append(wt)
			finalCharacters.append(pred)
		counter += 1

	# Reutrn your widths, heights and predicted characters
	return finalHeights, finalWidths, finalCharacters

# This method will fix those characters (mainly in the newer ID books)
# and ensure that if any H,U,K are picked up as different text, they are corrected
def fix_chars(hts, wts, preds,type):
	# Initialize all the variables needed
	characters = []
	heights = []
	widths = []
	counter = 0
	length = len(preds)
	# Ensure that we don't incur any out of index errors
	while counter != length:
		# Intialize new heights and weights of next character
		ht = hts[counter]
		wt = wts[counter]
		prd = preds[counter]
		if counter+1 != length:
			wt2 = wts[counter+1]
			changecount = 0
			if wt[0][0] < ht[0][0]/2.5 and prd != 'I':
				characters.append('I')
				widths.append(wt)
				heights.append(ht)
				changecount = 1
			# Try and fix the U first: common errors in these width ranges take place
			if (prd == 'L') and (preds[counter+1] == 'J' or preds[counter+1] == 'I'):
				# Use this range for new ID Books
				if wt[0][2]+2 >= wt2[0][1] and type == 0:
					characters.append('U')
					widths.append([[wt2[0][2] - wt[0][1], wt[0][1], wt2[0][2]]])
					heights.append(ht)
					counter += 1
					changecount = 1
				# Use this range for old ID Books
				elif wt[0][2]+2 >= wt2[0][1] and type == 1:
					characters.append('U')
					widths.append([[wt2[0][2] - wt[0][1], wt[0][1], wt2[0][2]]])
					heights.append(ht)
					counter += 1
					changecount = 1
			elif prd == 'H' and (preds[counter+1] == 'I' or preds[counter+1] == 'T'):
				# Use this range for new ID Books
				if wt[0][2]+4 >= wt2[0][1] and type == 0:
					characters.append('H')
					widths.append([[wt2[0][2] - wt[0][1], wt[0][1], wt2[0][2]]])
					heights.append(ht)
					counter += 1
					changecount = 1
				# Use this range for old ID Books
				elif wt[0][2]+1 >= wt2[0][1] and type == 1:
					characters.append('H')
					widths.append([[wt2[0][2] - wt[0][1], wt[0][1], wt2[0][2]]])
					heights.append(ht)
					counter += 1
					changecount = 1
			elif prd == 'I' and (preds[counter+1] == 'K' or preds[counter+1] == 'C'):
				if preds[counter+1] == 'C' and preds[counter-1] == 'R' and preds[counter-2] == 'F':
					changecount = 0
				elif preds[counter+1] == 'C' and preds[counter-1] == 'I' and (wt[0][1] <= wts[counter-1][0][2]+15):
					characters.append('K')
					widths.append([[wt2[0][2] - wt[0][1], wt[0][1], wt2[0][2]]])
					heights.append(ht)
					counter += 1
					changecount = 1
				# Use this range for new ID Books
				elif wt[0][2]+2 >= wt2[0][1] and type == 0:
					characters.append('K')
					widths.append([[wt2[0][2] - wt[0][1], wt[0][1], wt2[0][2]]])
					heights.append(ht)
					counter += 1
					changecount = 1
				# Use this range for old ID Books
				elif wt[0][2]+2 >= wt2[0][1] and type == 1:
					characters.append('K')
					widths.append([[wt2[0][2] - wt[0][1], wt[0][1], wt2[0][2]]])
					heights.append(ht)
					counter += 1
					changecount = 1

				# vowels = ['A','E','I','O','U']
				# if len(characters) > 1 and counter+1 < length and counter+2 < length-1:
				# 	if wts[counter-2][0][1] <= wts[counter-1][0][2]+15:
				# 		if ((characters[len(characters)-1] not in vowels) and (characters[len(characters)-2] not in vowels) and (preds[counter+1] not in vowels)) or ((preds[counter-1] not in vowels) and (preds[counter+2] not in vowels) and (preds[counter+1] not in vowels)):
				# 			characters.append('I')
				# 			widths.append(wt)
				# 			heights.append(ht)
				# 			changecount = 1
				# 		elif ((characters[len(characters)-1] == 'T') and ((preds[counter+1] in vowels) or (preds[counter+2] in vowels))):
				# 			characters.append('I')
				# 			widths.append(wt)
				# 			heights.append(ht)
				# 			changecount = 1
				# elif len(characters) == 1 and counter+1 < length and counter+2 < length-1:
				# 	if ((characters[0] not in vowels) and (preds[counter+1] not in vowels)) and (preds[counter+1] != 'T' or preds[counter-1] != 'T'):
				# 		characters.append('I')
				# 		widths.append(wt)
				# 		heights.append(ht)
				# 		changecount = 1
				# 	elif ((characters[0] not in vowels) and ((preds[counter+1] in vowels) or (preds[counter+2] in vowels))):
				# 		characters.append('I')
				# 		widths.append(wt)
				# 		heights.append(ht)
				# 		changecount = 1
				# elif len(characters) > 1 and counter+1 == length -1:
				# 	if ((characters[len(characters)-1] not in vowels) and (preds[counter+1] not in vowels) and (characters[len(characters)-2] not in vowels)):
				# 		characters.append('I')
				# 		widths.append(wt)
				# 		heights.append(ht)
				# 		changecount = 1
			# Ensure NO change has taken place before you add more incorrect digits
			if changecount == 0:
				characters.append(prd)
				widths.append(wt)
				heights.append(ht)
		# Add the last digit when the counter has reached the end of the line
		else:
			characters.append(prd)
			widths.append(wt)
			heights.append(ht)
		# Increase the counter to not get a endless looppython3.6 App.py -i "gift godongwana.jpg"
		counter += 1
	return heights, widths, characters

# Add spaces to the text where needed
def add_spaces(hts, wts, preds, type):
	# Intialize the varaibales needed
	counter = 0
	characters = []
	widths = []
	heights = []
	length = len(wts)
	# Run through the widths of the characters
	for wt in wts:
		# Append the next array item for: widths, heights and characters
		characters.append(preds[counter])
		widths.append(wt)
		heights.append(hts[counter])
		# Check if not end of array
		if counter + 1 != length:
			# If not, grab the next character and widths of that character
			wt2 = wts[counter+1]
			# Perform the tesst:
			tester = wt2[0][1] - wt[0][2]
			wds = []
			# (first character end and second character start point are
			# between 15 and 60 pixels away from eachother)
			if (tester < 60) and (tester >= (hts[counter][0][0]/2.5)):
				# Add space to characters
				characters.append(' ')
				# Calculate width of new space character adn append to widths
				wds.append([wt2[0][1] - wt[0][2], wt[0][2], wt2[0][1]])
				widths.append([wds[0]])
				# Use the same height as the previous text
				heights.append(hts[counter])

		# Add to the counter
		counter += 1

	# Return the heights, widths and new predicted character set
	return heights, widths, characters

# Use this to match character found in the model to their corresponding letter
Alphabet = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]

# Initialize counts
count = 0

# Method to start the extraction of BEST text in each region
def text_extraction_best(image, type):
	shape = image.shape
	height = 125
	r = height / float(shape[0])
	dim = (int(shape[1] * r), height)

	image = cv2.resize(image, dim, interpolation = cv2.INTER_AREA)

	# Start find_Text method
	hts, wts, preds, output, original = find_text(image, type, 0)

	# Start best_text method
	hts, wts, preds = best_text(hts, wts, preds,original, type)

	# Start fix_chars method
	hts, wts, preds = fix_chars(hts, wts, preds, type)

	# Start add_spaces method
	hts, wts, preds = add_spaces(hts, wts, preds, type)
	
	# Create new prediction string and return the string
	prediction = ''
	for pred in preds:
		prediction = prediction + pred

	return prediction, output