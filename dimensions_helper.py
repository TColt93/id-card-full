def get_dimensions(value):
    # Initialize the return arrays
    RoIs = []
    Text = []
    Emblems = []

    # Dimensions of the various RoIs in the ID book
    RoIs.append([0.203703704,0.949074074,0.226470588,0.656470588])

    # Dimensions for the barcode RoI and the digits needed for validations
    Text.append([0,0.161490683,0,1])
    Text.append([0.161490683,0.285714286,0,1])
    Text.append([0.285714286,0.403726708,0,1])
    Text.append([0.403726708,0.52173913,0,1])
    Text.append([0.52173913,0.639751553,0,1])
    Text.append([0.639751553,0.757763975,0,1])
    Text.append([0.757763975,0.875776398,0,1])
    Text.append([0.875776398,1,0,1])

    # Dimensions for the 4 Emblem image areas on the ID Book
    Emblems.append([0.708333333,1,0,0.576470588])
    Emblems.append([0.481481481,0.708333333,0,0.576470588])

    if value == 1:
        return RoIs
    if value == 2:
        return Text
    if value == 3:
        return Emblems
    if value == 4:
        return RoIs, Text, Emblems
