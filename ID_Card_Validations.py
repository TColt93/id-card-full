from digit_extraction import digit_extraction_full as digits_extract
from cv2 import cv2
import os
import numpy as np
import datetime
from skimage.measure import compare_ssim
import json
import re
import imutils
import sys
from PIL import Image
import dimensions_helper as dimensions
import Boundary_helper as bounds

# Initialize strings
barcodeTrue = ''
DateTrue = ''

# Create Variables for the paths and image areas
Emblempath = 'Templates/Emblem.png'

Regions = ['CoB', 'DoI', 'DoB', 'Name', 'Sur']

# Initialize the list variables
RoIs = []
Threshed = []
jsonText = []
files = []
RoICompareResult = []
templates = []
finalResultSet = []

# Functionality for extracting RoIs from the ID Book
def Extract_ROIs(image):
    img = image
    Threshed = []
    # Get the dynamic dimensions from the get_dimensions function
    RoIs = dimensions.get_dimensions(1)
    Text = dimensions.get_dimensions(2)
    TextDim = Text

    SurnameDim = Text[0]
    NameDim = Text[1]
    SexDim = Text[2]
    NationalityDim = Text[3]
    IdDim = Text[4]
    DobDim = Text[5]
    CobDim = Text[6]
    StatusDim = Text[7]

    # Get ID width and height
    (h, w) = img.shape[:2]

    # Extract Text Image from the Text Dimensions
    TextPic = img[round(int(h)*TextDim[0]) : round(int(h)*TextDim[1]), round(int(w)*TextDim[2]) : round(int(w)*TextDim[3])]

    (h, w) = TextPic.shape[:2]

    # Extract Info Images from the Text Image
    SurnamePic = TextPic[round(int(h)*SurnameDim[0]) : round(int(h)*SurnameDim[1]), round(int(w)*SurnameDim[2]) : round(int(w)*SurnameDim[3])]
    NamePic = TextPic[round(int(h)*NameDim[0]) : round(int(h)*NameDim[1]), round(int(w)*NameDim[2]) : round(int(w)*NameDim[3])]
    SexPic = TextPic[round(int(h)*SexDim[0]) : round(int(h)*SexDim[1]), round(int(w)*SexDim[2]) : round(int(w)*SexDim[3])]
    NationalityPic = TextPic[round(int(h)*NationalityDim[0]) : round(int(h)*NationalityDim[1]), round(int(w)*NationalityDim[2]) : round(int(w)*NationalityDim[3])]
    IdPic = TextPic[round(int(h)*IdDim[0]) : round(int(h)*IdDim[1]), round(int(w)*IdDim[2]) : round(int(w)*IdDim[3])]
    DobPic = TextPic[round(int(h)*DobDim[0]) : round(int(h)*DobDim[1]), round(int(w)*DobDim[2]) : round(int(w)*DobDim[3])]
    CobPic = TextPic[round(int(h)*CobDim[0]) : round(int(h)*CobDim[1]), round(int(w)*CobDim[2]) : round(int(w)*CobDim[3])]
    StatusPic = TextPic[round(int(h)*StatusDim[0]) : round(int(h)*StatusDim[1]), round(int(w)*StatusDim[2]) : round(int(w)*StatusDim[3])]

    RoIs = [SurnamePic, NamePic, SexPic, NationalityPic, IdPic, DobPic, CobPic, StatusPic]

    counter = 0
    test = False
    for img in RoIs:
        if counter == 0:
            boundary = bounds.Get_Best_Boundaries(img, 5, 3, 10, 11, test)
            Threshed.append(bounds.ROI_Thresh(img, boundary))

        elif counter == 1:
            boundary = bounds.Get_Best_Boundaries(img, 5, 3, 10, 12, test)
            Threshed.append(bounds.ROI_Thresh(img, boundary))

        elif counter == 2:
            boundary = bounds.Get_Best_Boundaries(img, 8, 4, 4, 6.2, test)
            Threshed.append(bounds.ROI_Thresh(img, boundary))

        elif counter == 3:
            boundary = bounds.Get_Best_Boundaries(img, 4, 3.2, 4.4, 14.5, test)
            Threshed.append(bounds.ROI_Thresh(img, boundary))

        elif counter == 4:
            boundary = bounds.Get_Best_Boundaries(img, 2, 3.2, 4.4, 26, test)
            Threshed.append(bounds.ROI_Thresh(img, boundary))

        elif counter == 5:
            boundary = bounds.Get_Best_Boundaries(img, 2, 4.5, 4.5, 19, test)
            Threshed.append(bounds.ROI_Thresh(img, boundary))

        elif counter == 6:
            boundary = bounds.Get_Best_Boundaries(img, 4, 4.5, 4.5, 14.5, test)
            Threshed.append(bounds.ROI_Thresh(img, boundary))

        elif counter == 7:
            boundary = bounds.Get_Best_Boundaries(img, 4, 5, 4.5, 14, test)
            Threshed.append(bounds.ROI_Thresh(img, boundary))

        counter += 1

    return Threshed

# Functionality for finding the emblems and counting how many correct matches in the ID Book
def find_and_validate_emblems(image):
    
    boundaries, emblem_boundaries, barcode_boundaries = bounds.get_boundaries(3)

    RoIs, Barcodes, Emblems = dimensions.get_dimensions(3)
    idEmblem = Emblems[0]
    brailEmblem = Emblems[1]

    totalFoundCount = 0
    EmblemPics = []
    boundary = []

    img = image
    (h, w) = img.shape[:2]

    EmblemPics.append(img[round(int(h)*idEmblem[0]) : round(int(h)*idEmblem[1]), round(int(w)*idEmblem[2]) : round(int(w)*idEmblem[3])])
    EmblemPics.append(img[round(int(h)*brailEmblem[0]) : round(int(h)*brailEmblem[1]), round(int(w)*brailEmblem[2]) : round(int(w)*brailEmblem[3])])
    count = 0
    for img in EmblemPics:

        boundary.append(emblem_boundaries[count])
        if count == 0:
            min = 700000
        else:
            min = 500000

        found = [0]
        foundCount = 0

        for (lower, upper) in emblem_boundaries:
            # create NumPy arrays from the boundaries
            lower = np.array(lower, dtype = 'uint8')
            upper = np.array(upper, dtype = 'uint8')

            # find the colors within the specified boundaries and apply the mask
            mask = cv2.inRange(img, lower, upper)
            img = cv2.bitwise_and(img, img, mask = mask)

            kernel = np.ones((2,2),np.uint8)
            img = cv2.dilate(img,kernel,iterations = 1)

            img = cv2.bitwise_not(img)
            ret, img = cv2.threshold(img, 120, 255, cv2.THRESH_BINARY)
            img = cv2.bitwise_not(img)

            cv2.imshow('img', img)
            cv2.waitKey(0)
            cv2.destroyAllWindows()
            
        #Get image dimensions for template resizing and get canny edges for ROI in image
        (w, h) = img.shape[:2]
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img = cv2.Canny(img, 50, 200)

        #Template read and manipulate to fit Image dimensions and get canny edges of template image
        template = cv2.imread(Emblempath)
        width = round(w*0.3)
        height = round(h*0.5)
        template = cv2.resize(template, (width, height))
        template = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)
        template = cv2.Canny(template, 50, 200)
        (tH, tW) = template.shape[:2]

        for scale in np.linspace(0.2, 1.0, 20)[::-1]:
            # resize the image according to the scale, and keep track of the ratio of the resizing
            resized = imutils.resize(img, width = int(img.shape[1] * scale))
            r = img.shape[1] / float(resized.shape[1])

            # if the resized image is smaller than the template, then break from the loop
            if resized.shape[0] < tH or resized.shape[1] < tW:
                break

            # detect edges in the resized, grayscale image and apply template matching to find the template in the image
            result = cv2.matchTemplate(resized, template, cv2.TM_CCOEFF)
            (_, maxVal, _, maxLoc) = cv2.minMaxLoc(result)

            # if we have found a new maximum correlation value, then update the bookkeeping variable
            if found is None or maxVal > found[0]:
                found = (maxVal, maxLoc, r)

        # unpack the bookkeeping varaible and compute the (x, y) coordinates of the bounding box based on the resized ratio
        if found[0] > min:
            foundCount = foundCount + 1

        totalFoundCount = totalFoundCount + foundCount
    return totalFoundCount

# Functionality for validating the dates and numbers within the ID Book
def Date_Nationality_Validation(image, DoBimage, Type):

    RoIs, Barcodes, Emblems = dimensions.get_dimensions(3)
    BarcodesDim = Barcodes[0]
    DigitsDim = Barcodes[1]
    LastThreeDim = Barcodes[2]
    DateCheckDim = Barcodes[3]
    output = []

    #Extract all the RoIs from the Barcode
    (h, w) = image.shape[:2]
    BarcodePicOrig = image[round(int(h)*BarcodesDim[Type][0]) : round(int(h)*BarcodesDim[Type][1]), round(int(w)*BarcodesDim[Type][2]) : round(int(w)*BarcodesDim[Type][3])]
    BarcodePic = Barcode_Thresh(BarcodePicOrig)
    (h2, w2) = BarcodePic.shape[:2]
    DigitsPic = BarcodePic[round(int(h2)*DigitsDim[Type][0]) : round(int(h2)*DigitsDim[Type][1]), round(int(w2)*DigitsDim[Type][2]) : round(int(w2)*DigitsDim[Type][3])]
    (h3, w3) = DigitsPic.shape[:2]
    LastThreePic = DigitsPic[round(int(h3)*LastThreeDim[Type][0]) : round(int(h3)*LastThreeDim[Type][1]), round(int(w3)*LastThreeDim[Type][2]) : round(int(w3)*LastThreeDim[Type][3])]
    DateCheckPic = DigitsPic[round(int(h3)*DateCheckDim[Type][0]) : round(int(h3)*DateCheckDim[Type][1]), round(int(w3)*DateCheckDim[Type][2]) : round(int(w3)*DateCheckDim[Type][3])]
    
    # Get the DoB from the ID and the Barcode areas
    barDoB, out = barDoB_Fetch(DateCheckPic, Type)
    output.append(out)
    DoB, out = DoB_Fetch(DoBimage, Type)
    output.append(out)

    # If either DoB cannot be read, don't bother about the date check method
    if barDoB == 'Could Not Validate' or DoB == 'Could Not Validate':
        DateCheck = 'Could Not Validate'
    # If both contain digits, perform the date check
    else:
        DateCheck = date_check(barDoB, DoB)

    LastThreeResult, out1 = ExtraDocument_Validate(LastThreePic, Type)
    output.append(out1)

    Gender = Gender_Validate(DateCheckPic, Type)

    Nationality = Nationality_Validate(LastThreePic, Type)

    return BarcodePicOrig, DateCheck, LastThreeResult, Gender, Nationality, output

def Barcode_Thresh(img):

    boundaries, emblem_boundaries, barcode_boundaries = bounds.get_boundaries(3)

    for (lower, upper) in barcode_boundaries:
        lower = np.array(lower, dtype = 'uint8')
        upper = np.array(upper, dtype = 'uint8')

        (h,w) = img.shape[:2]
        blank_image = np.zeros((h,w,3), np.uint8)

        # find the colors within the specified boundaries and apply the mask
        mask = cv2.inRange(img, lower, upper)
        img = cv2.bitwise_not(img, blank_image, mask = mask)

        ret, img = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY_INV)
    return img

# Functionality for extracting the digits from the barcode DoB area
def barDoB_Fetch(barDoB, type):
    barcodeTrue = 'True'
    opbar = []
    digits, out = digits_extract(barDoB, type)
    
    # cv2.imshow('image',out)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    opbar.append(["Barcode DoB Image", out])
    # find length of digits string
    length = len(digits)
    # Move onto the next one if cannot find any digits
    if length == 0 or length == 1:
        barcodeTrue = 'Could Not Validate'
        return barcodeTrue, opbar

    matcher = 0
    cntr = 0
    while cntr is not 2:
        matchspace = re.search(r' ', digits)
        if matchspace is not None:
            m1 = digits[matchspace.regs[0][0]]
            m2 = digits[matchspace.regs[0][1]]
            if  m2 == ' ':
                matcher = matchspace.regs[0][1]
            elif m1 == ' ':
                matcher = matchspace.regs[0][0]
            if cntr == 1:
                digits = digits[:matcher]
            else:
                before = len(digits[:matcher])
                after = len(digits[matcher:])
                if before > after:
                    digits = digits[:matcher]
                else:
                    digits = digits[matcher+1:]
        cntr += 1

    #Make sure that we are focusing on the right second last digit
    length = len(digits)
    if length > 6:
        digits = digits[:6]

    if length < 6:
        barcodeTrue = 'Could Not Validate'

    if barcodeTrue != 'Could Not Validate':
        if digits[0] == '0':
            digits = '20' + digits[0] + digits[1] + '-' + digits[2] + digits[3] + '-' + digits[4] + digits[5]
            barcodeTrue = digits
            return barcodeTrue, opbar
        else:
            digits = '19' + digits[0] + digits[1] + '-' + digits[2] + digits[3] + '-' + digits[4] + digits[5]
            barcodeTrue = digits
            return barcodeTrue, opbar

    return barcodeTrue, opbar

# Functionality for extracting the digits from the DoB area
def DoB_Fetch(DoB, type):
    DateTrue = 'True'
    opdate = []
    digits, out = digits_extract(DoB, type)
    
    # cv2.imshow('image',out)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    opdate.append(["Body DoB Image", out])
    # find length of digits string
    length = len(digits)
    # Move onto the next one if cannot find any digits
    if length == 0 or length == 1:
        DateTrue = 'Could Not Validate'
        return DateTrue

    digits = digits.replace(' ', '')

    length = len(digits)

    if length < 6:
        DateTrue = 'Could Not Validate'
        return DateTrue, opdate

    if  DateTrue != 'Could Not Validate':
        DateTrue = digits
        return DateTrue, opdate

def date_check(barcodeTrue, DateTrue):
    DateCheck = 'Could Not Validate'
    if barcodeTrue == DateTrue:
        DateCheck = 'True'
        return DateCheck
    bar = barcodeTrue.replace('-','')
    date = DateTrue.replace('-','')
    if bar == date:
        DateCheck = 'True'
        return DateCheck
    lengthD = len(date)
    lengthB = len(bar)
    if lengthD == 10:
        date0 = date[:4]+date[5:7]+date[lengthD-2:]
        if bar == date0:
            DateCheck = 'True'
            return DateCheck
        else:
            count = 0
            charCount = 0
            for Bchar in bar:
                if Bchar == date[charCount]:
                    count += 1
                    charCount += 1
                    continue
                elif Bchar == date[charCount+1] and charCount != 9:
                    count += 1
                    charCount += 2
                else:
                    charCount += 1
        if count > 6:
            DateCheck = 'True*'
    if lengthD == 9:
        date1 = date[:6]+date[lengthD-2:]
        if bar == date1:
            DateCheck = 'True'
            return DateCheck
        date1 = date[:4]+date[5:]
        if bar == date1:
            DateCheck = 'True'
            return DateCheck
        else:
            count = 0
            charCount = 0
            for Bchar in bar:
                d = date[charCount]
                if Bchar == d:
                    count += 1
                    charCount += 1
                    continue
                d1 = date[charCount + 1]
                if Bchar == d1 and charCount != 8:
                    count += 1
                    charCount += 2
                else:
                    charCount += 1
        if count > 6:
            DateCheck = 'True*'
    if lengthD == 8:
        if bar == date:
            DateCheck = 'True'
            return DateCheck
        else:
            count = 0
            charCount = 0
            for Bchar in bar:
                if Bchar == date[charCount]:
                    count += 1
                    charCount += 1
                    continue
                if charCount != 7:
                    if Bchar == date[charCount+1]:
                        count += 1
                        charCount += 2
                else:
                    charCount += 1
        if count > 6:
            DateCheck = 'True*'
    date2 = date[:2]+date[3:5]+date[lengthD-2:]
    bar2 = bar[:2]+bar[4:6]+bar[lengthB-2:]
    if bar2 == date2:
        DateCheck = 'True'
        return DateCheck
    date3 = date[:4]+date[5]+date[lengthD-2:]
    bar3 = bar[:4]+bar[5]+date[lengthB-2:]
    if bar3 == date3:
        DateCheck = 'True'
        return DateCheck
    return DateCheck

def ExtraDocument_Validate(LastThreeImg, type):
    output = []
    digits, out = digits_extract(LastThreeImg, type)
    output.append(["Last Three Digits", out])
    # find length of digits string
    length = len(digits)
    # Move onto the next one if cannot find any digits
    if length == 0 or length == 1:
        result = 'Could Not Validate'
        return result
    matchspace = re.search(r' ', digits)
    matchdot = re.search(r'.', digits)
    matchdash = re.search(r'-', digits)
    if (matchspace is not None) or (matchdot is not None) or (matchdash is not None):
        digits = digits.replace(' ', '')
        digits = digits.replace('.', '')
        digits = digits.replace('-', '')

    #Make sure that we are focusing on the right second last digit
    length = len(digits)
    secondLast = digits[(length - 2)]

    if secondLast == '8':
        result = 'NO'
        return result, output
    elif secondLast == '9':
        result = 'YES'
        return result, output
    else:
        result = 'Could not Validate'
        return result, output

def Gender_Validate(DateCheckPic, type):
    digits, out = digits_extract(DateCheckPic, type)
    # find length of digits string
    length = len(digits)
    # Move onto the next one if cannot find any digits
    if length == 0 or length == 1:
        result = 'Could not Validate'
        return result
    
    digits = digits[length-4:]

    matchspace = re.search(r' ', digits)
    if matchspace is not None:
        m1 = digits[matchspace.regs[0][0]]
        m2 = digits[matchspace.regs[0][1]]
        if  m2 == ' ':
            matcher = matchspace.regs[0][1]
        elif m1 == ' ':
            matcher = matchspace.regs[0][0]
        digits = digits[matcher+1:]

    #Make sure that we are focusing on the right second last digit
    length = len(digits)
    firstDigit = digits[0]

    if firstDigit < '5':
        result = 'Female'
        return result
    elif firstDigit >= '5':
        result = 'Male'
        return result
    elif firstDigit == ' ':
        firstDigit = digits[1]
        if firstDigit < '5':
            result = 'Female'
            return result
        elif firstDigit >= '5':
            result = 'Male'
            return result

def Nationality_Validate(LastThreeImg, type):
    digits, out = digits_extract(LastThreeImg, type)
    # find length of digits string
    length = len(digits)
    # Move onto the next one if cannot find any digits
    if length == 0 or length == 1:
        result = 'Could not Validate'
        return result
    matchspace = re.search(r' ', digits)
    if (matchspace is not None):
        digits = digits.replace(' ', '')

    #Make sure that we are focusing on the right second last digit
    length = len(digits)
    ThirdLast = digits[(length - 3)]

    if ThirdLast == '0':
        result = 'SA Citizen'
        return result
    elif ThirdLast == '1':
        result = 'Permanent Residence'
        return result
    else:
        result = 'Could not Validate'
        return result

def Validate_ID(file):
    #Read in image
    imgLoad = file

    # ROIs = Extract_ROIs(imgLoad)
    Emblemsfound = find_and_validate_emblems(imgLoad)
    Barcode, DateCheck, LastThreeResult, Gender, Nationality, output = Date_Nationality_Validation(imgLoad, Threshed[2], 0)

    finalResultSet.append(['EmblemsFound', str(Emblemsfound)])
    finalResultSet.append(['DateCheck', DateCheck])
    finalResultSet.append(['ExtraDocsNeeded', LastThreeResult])
    finalResultSet.append(['BarcodeFound', 'No Barcode on Front of ID Card'])
    finalResultSet.append(['Gender', Gender])
    finalResultSet.append(['Nationality', Nationality])

    count = 0
    notes = []
    TotalFailed = 0
    for check in finalResultSet:
        Failed = 0
        if count == 0:
            if check[1] == '4':
                continue
            if check[1] == '3':
                notes.append('Only 3 Emblems Found')
            if check[1] == '2':
                notes.append('Only 2 Emblems Found')
            if check[1] == '1' or check[1] == '0':
                notes.append('Not enough Emblems Found')
                TotalFailed += 1
        elif check[1] == 'Could Not Validate':
            Failed == 1
            TotalFailed += 1
        if Failed == 1:
            if count == 1:
                notes.append('Could not Validate the Date of Birth')
            if count == 2:
                notes.append('Could not Validate if there is extra documentation needed')
            if count == 3:
                notes.append('Could not Find the Barcode to enough of a degree')
            if count == 4:
                notes.append('Could not Validate Gender')
            if count == 5:
                notes.append('Could not Validate Nationality')
        count += 1    
    
    if TotalFailed == 0:
        notes.append('All validations have been passed')

    finalResultSet.append(['Tests Passed', str(6-TotalFailed)])
    finalResultSet.append(['notes', notes])
    finalResultSet.append(['DateTime', str(datetime.datetime.now())])

    # return consolidated results from the functions above in array form
    return finalResultSet, output