import numpy as np
from cv2 import cv2

def get_boundaries(value):
    boundaries = [([0,0,0],[160,160,160])]

    emblem_boundaries = [([80,50,115],[150,140,170]),([0,0,0],[50,50,50])]

    barcode_boundaries= [([0,0,0],[156,156,156])]

    if value == 1:
        return boundaries
    if value == 2:
        return emblem_boundaries
    if value == 3:
        return barcode_boundaries
    if value == 4:
        return boundaries, emblem_boundaries, barcode_boundaries

def ROI_Thresh(imgLoad, boundary):
    for (lower, upper) in boundary:
        # create NumPy arrays from the boundaries
        lower = np.array(lower, dtype = 'uint8')
        upper = np.array(upper, dtype = 'uint8')

        (h,w) = imgLoad.shape[:2]
        blank_image = np.zeros((h,w,3), np.uint8)

        # find the colors within the specified boundaries and apply the mask
        mask = cv2.inRange(imgLoad, lower, upper)
        img = cv2.bitwise_not(imgLoad, blank_image, mask = mask)

        # Threshold RoI for sorting
        ret, img = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY_INV)

    Threshed = img
    return Threshed

# Threshold the ROIs using colour coundaries
def Get_Best_Boundaries(TextImg, divw, divh1, divh2, goodscr, test):
    score = 0
    counter = 0
    boundaries = get_boundaries(1)
    boundary = []
    boundary.append(boundaries[0])
    imgLoad = TextImg
    prevscore = 0

    (h,w) = imgLoad.shape[:2]

    cropped = imgLoad[int(h/divh1):int(h - h/divh2), 0:int(w/divw)]
    # blank = blank_image_create(cropped)

    while counter!= 120:
        if counter > 0:
            upper = boundary[0][1]
            c = 0
            for item in upper:
                boundary[0][1][c] = item - 1
                c += 1
        
        Threshed = ROI_Thresh(cropped, boundary)

        number = get_black_perc(Threshed)

        h,w = Threshed.shape[:2]
        percOfBlack = number/(h*w)*100

        if test == True:
            print(str(score) + ' - ' + str(percOfBlack) + '%')

            cv2.imshow('Threshed', Threshed)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

        # if score-prevscore < 0.00005 and score < 0.99 and score > goodscr*90/100:
        #     break

        lower = goodscr*90/100
        higher = goodscr*110/100

        if percOfBlack >= lower and percOfBlack <= higher:
            break

        counter += 1
        prevscore = score
    # print(str(score) + ' - ' + str(percOfBlack))
    return boundary

def get_black_perc(img):    
    # Compare the differences of the blank image and the ROI

    colorLow = np.array([0, 0, 0], np.uint8)
    colorHigh = np.array([10, 10, 10], np.uint8)

    dst = cv2.inRange(img, colorLow, colorHigh)

    number = cv2.countNonZero(dst)

    # (score, diff) = compare_ssim(img, img2, full=True, multichannel=True)
    # diff = (diff * 255).astype("uint8")

    return number


def blank_image_create(imgLoad):

    (h, w) = imgLoad.shape[:2]
    # Create blank image with the same size as the given image
    blank = np.zeros((h,w,3), np.uint8)
    blank[0:h,0:w] = (255,255,255)
    
    # Threshold the blank template
    ret, blank = cv2.threshold(blank, 100, 255, cv2.THRESH_BINARY)

    return blank